package com.patrycjacieciera.vetclinic.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindClientResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
}
