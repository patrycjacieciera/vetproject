package com.patrycjacieciera.vetclinic.disease;

import com.patrycjacieciera.vetclinic.diagnosis.DiagnosisNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DiseaseService {

    private final DiseaseRepository diseaseRepository;

    public Long createDisease(CreateDiseaseRequest request){
        Disease disease = buildDiseaseEntity(request);
        diseaseRepository.save(disease);
        return disease.getId();
    }

    private Disease buildDiseaseEntity(CreateDiseaseRequest request){
        Disease disease = new Disease();
        disease.setName(request.getName());
        return disease;
    }

    public Disease getDisease(Long id){
        return diseaseRepository.findById(id).orElseThrow(()-> new DiseaseNotFoundException(id));
    }

}