package com.patrycjacieciera.vetclinic.bill;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BillService {

    private final BillRepository billRepository;

    public Long createBill(CreateBillRequest request){
        Bill bill = buildBillEntity(request);
        billRepository.save(bill);

        return bill.getId();
    }

    private Bill buildBillEntity(CreateBillRequest request){
        Bill bill = new Bill();
        bill.setCost(request.getCost());
        return bill;
    }

    public Bill getBill(Long id){
        return billRepository.findById(id).orElseThrow(() -> new BillNotFoundException(id));
    }

}
