package com.patrycjacieciera.vetclinic.bill;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
class CreateBillRequest {

    @NotNull
    private double cost;

}
