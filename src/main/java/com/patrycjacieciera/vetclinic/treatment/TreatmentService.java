package com.patrycjacieciera.vetclinic.treatment;

import com.patrycjacieciera.vetclinic.medicine.Medicine;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TreatmentService {

    private final TreatmentRespiratory treatmentRespiratory;

    public Long createTreatment(CreateTreatmentRequest request){
        Treatment treatment = buildTreatmentEntity(request);
        treatmentRespiratory.save(treatment);
        return treatment.getId();
    }

    private Treatment buildTreatmentEntity(CreateTreatmentRequest request){
        Treatment treatment = new Treatment();
        treatment.setQuantity(request.getQuantity());
        Medicine medicine = buildMedEntity(request.getMedcineDto());
        treatment.getMedicines().add(medicine);
        return treatment;
    }

    private Medicine buildMedEntity(MedicineDto medicineDto){
        Medicine medicine = new Medicine();
        medicine.setName(medicineDto.getName());
        medicine.setPrice(medicineDto.getPrice());
        return medicine;
    }

    public Treatment getTreatment(Long id){
        return treatmentRespiratory.findById(id).orElseThrow(()-> new TreatmentNotFoundException(id));
    }
}
