package com.patrycjacieciera.vetclinic.treatment;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/treatment")
@RequiredArgsConstructor
public class TreatmentController {

    private final TreatmentService treatmentService;

    @PostMapping
    public Long addTreatment(@Valid @RequestBody CreateTreatmentRequest request){
        return treatmentService.createTreatment(request);
    }

    @GetMapping("/{id}")
    public Treatment getTreatment (@PathVariable ("id") Long id){
        return treatmentService.getTreatment(id);
    }
}