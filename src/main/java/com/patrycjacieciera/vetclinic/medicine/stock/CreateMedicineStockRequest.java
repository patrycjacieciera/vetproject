package com.patrycjacieciera.vetclinic.medicine.stock;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateMedicineStockRequest {

    @NotNull
    private double quantity;
}
