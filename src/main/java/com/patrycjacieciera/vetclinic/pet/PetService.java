package com.patrycjacieciera.vetclinic.pet;

import com.patrycjacieciera.vetclinic.breed.BreedService;
import com.patrycjacieciera.vetclinic.client.Client;
import com.patrycjacieciera.vetclinic.client.ClientService;
import com.patrycjacieciera.vetclinic.diagnosis.Diagnosis;
import com.patrycjacieciera.vetclinic.disease.Disease;
import com.patrycjacieciera.vetclinic.disease.DiseaseService;
import com.patrycjacieciera.vetclinic.vet.Vet;
import com.patrycjacieciera.vetclinic.vet.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PetService {

    private final PetRepository petRepository;
    private final ClientService clientService;
    private final BreedService breedService;
    private final DiseaseService diseaseService;
    private final VetService vetService;

    @Transactional
    public Long createPet(CreatePetRequest request) {

        Pet pet = buildPetEntity(request);
        petRepository.save(pet);
        return pet.getId();
    }

    private Pet buildPetEntity (CreatePetRequest request){
        Pet pet = new Pet();
        pet.setName(request.getName());
        pet.setRace(request.getRace());
        pet.setSex(request.getSex());
        pet.setColor(request.getColor());
        pet.setPurebred(request.isPurebred());
        pet.setCastrated(request.isCastrated());
        pet.setAlive(request.isAlive());
       // pet.setBreed(breedService.getBreed(request.getBreedId()));
        Client client = clientService.getClient(request.getOwnerId());
        pet.setOwner(client);

        return pet;
    }

    private Diagnosis buildDiagnosisEntity (DiagnosisDto diagnosisDto){
        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setDescription(diagnosisDto.getDescription());
        diagnosis.setComments(diagnosisDto.getComments());
        diagnosis.setDate(LocalDate.now());

        Disease disease = diseaseService.getDisease(diagnosisDto.getDiseaseId());
        diagnosis.setDisease(disease);

        Vet vet = vetService.getVet(diagnosisDto.getVetId());
        diagnosis.setVet(vet);
        return diagnosis;
    }

    public Pet getPet(Long id) {
        return petRepository.findById(id).orElseThrow(()-> new PetNotFoundException(id));
    }

   public Pet getPetByName(String name){
        List <Pet> pet = petRepository.findByName(name);
       Iterator iterator = pet.iterator();

       for (Pet list: pet) {
           while (iterator.hasNext()){
               System.out.println(iterator.next());
           }
       }
        return pet.get(0);
   }

}
