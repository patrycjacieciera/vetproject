package com.patrycjacieciera.vetclinic.client;

import com.patrycjacieciera.vetclinic.pet.Pet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public Long clientCreate(CreateClientRequest request) {
        Client client = buildClientEntity(request);
        clientRepository.save(client);
        return client.getId();
    }

    private Client buildClientEntity(CreateClientRequest request) {
        Client client = new Client();
        client.setFirstName(request.getFirstName());
        client.setLastName(request.getLastName());
        client.setAddress(request.getAddress());
        client.setEmail(request.getEmail());
        client.setPassword(request.getPassword());
        Pet pet = buildPetEntity(request.getPetDto());
        pet.setOwner(client);
        client.getPets().add(pet);

        return client;
    }

    private Pet buildPetEntity(PetDto petDto) {
        Pet pet = new Pet();
        pet.setName(petDto.getName());
        pet.setRace(petDto.getRace());
        pet.setSex(petDto.getSex());
        pet.setColor(petDto.getColor());
        pet.setPurebred(petDto.isPurebred());
        pet.setCastrated(petDto.isCastrated());
        pet.setAlive(petDto.isAlive());
        return pet;
    }

    public Client getClient(Long id) {
        return clientRepository.findById(id).orElseThrow(()-> new ClientNotFoundException(id));
    }

    public Client getClientByLastName(String lastName){
        List<Client> list = clientRepository.findByLastName(lastName);
        return list.get(0);
    }

}
