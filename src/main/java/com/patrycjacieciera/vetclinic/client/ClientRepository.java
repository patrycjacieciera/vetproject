package com.patrycjacieciera.vetclinic.client;

import com.patrycjacieciera.vetclinic.pet.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("SELECT t FROM Client t WHERE t.lastName like %?1")
    List <Client> findByLastName(String lastName);
}