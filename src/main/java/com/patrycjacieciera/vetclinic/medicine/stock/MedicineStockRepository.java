package com.patrycjacieciera.vetclinic.medicine.stock;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineStockRepository extends JpaRepository<MedicineStock, Long> {
}
