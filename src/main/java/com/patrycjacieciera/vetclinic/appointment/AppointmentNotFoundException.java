package com.patrycjacieciera.vetclinic.appointment;

public class AppointmentNotFoundException  extends RuntimeException{

    public AppointmentNotFoundException(Long id){
        super(String.format("Appointment with id %d not found", id));
    }
}
