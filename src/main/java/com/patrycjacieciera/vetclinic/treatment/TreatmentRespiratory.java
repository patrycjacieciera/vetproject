package com.patrycjacieciera.vetclinic.treatment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TreatmentRespiratory extends JpaRepository<Treatment, Long> {
}
