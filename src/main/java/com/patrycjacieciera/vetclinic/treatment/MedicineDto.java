package com.patrycjacieciera.vetclinic.treatment;

import lombok.Data;

@Data
public class MedicineDto {

    private String name;
    private double price;

}
