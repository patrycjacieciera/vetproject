package com.patrycjacieciera.vetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.TimeZone;

@SpringBootApplication
public class VetclinicApplication {

    public static void main(String[] args) {
        SpringApplication.run(VetclinicApplication.class, args);
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}