package com.patrycjacieciera.vetclinic.bill;

public class BillNotFoundException extends RuntimeException {

    public BillNotFoundException (Long id){
        super(String.format("Bill with id %d not found", id));
    }
}
