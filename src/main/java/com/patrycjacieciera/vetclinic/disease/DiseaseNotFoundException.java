package com.patrycjacieciera.vetclinic.disease;

public class DiseaseNotFoundException extends RuntimeException {
    public DiseaseNotFoundException(Long id){
        super(String.format("Disease with id %d not found", id));
    }
}
