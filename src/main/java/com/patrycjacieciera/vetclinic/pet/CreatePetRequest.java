package com.patrycjacieciera.vetclinic.pet;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
class CreatePetRequest {

    @NotBlank
    private String name;
    @NotBlank
    private String race;

    private Sex sex;

    @NotBlank
    private String color;

    @NotNull
    private boolean purebred;

    @NotNull
    private boolean castrated;

    @NotNull
    private boolean alive;

    // @NotNull
   // private Long breedId;

    @NotNull
    private Long ownerId;
}
