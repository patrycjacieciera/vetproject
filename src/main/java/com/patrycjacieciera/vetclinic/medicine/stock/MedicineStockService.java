package com.patrycjacieciera.vetclinic.medicine.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MedicineStockService {

    private final MedicineStockRepository medicineStockRepository;

    public Long createMedicineStock(CreateMedicineStockRequest request){
        MedicineStock medicineStock = buildMedStockEntity(request);
        medicineStockRepository.save(medicineStock);
        return medicineStock.getId();
    }

    private MedicineStock buildMedStockEntity (CreateMedicineStockRequest request){
        MedicineStock medicineStock = new MedicineStock();
        medicineStock.setQuantity(request.getQuantity());
        return  medicineStock;
    }

    public MedicineStock getMedicineStock(Long id){
        return medicineStockRepository.findById(id).orElseThrow(()-> new MedStockNotFoundException(id));
    }
}
