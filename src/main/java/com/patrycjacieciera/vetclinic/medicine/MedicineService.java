package com.patrycjacieciera.vetclinic.medicine;

import com.patrycjacieciera.vetclinic.medicine.stock.MedicineStock;
import com.patrycjacieciera.vetclinic.medicine.stock.MedicineStockService;
import com.patrycjacieciera.vetclinic.treatment.Treatment;
import com.patrycjacieciera.vetclinic.treatment.TreatmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MedicineService {

    private final MedicineRepository medicineRepository;
    private final TreatmentService treatmentService;
    private final MedicineStockService medicineStockService;


    public Long createMedicine(CreateMedicineRequest request){
        Medicine medicine = buildMedicineEntity(request);

        medicineRepository.save(medicine);
        return medicine.getId();
    }

    private Medicine buildMedicineEntity(CreateMedicineRequest request){
        Medicine medicine = new Medicine();
        medicine.setName(request.getName());
        medicine.setPrice(request.getPrice());

        Treatment treatment = treatmentService.getTreatment(request.getTreatmenetId());
        MedicineStock medicineStock = medicineStockService.getMedicineStock(request.getStockId());

        medicine.setTreatByMed(treatment);
        medicine.setStock(medicineStock);

        return medicine;
    }

    public Medicine getMedicine(Long id){
        return medicineRepository.findById(id).orElseThrow(()-> new MedicineNotFoundException(id));
    }
}
