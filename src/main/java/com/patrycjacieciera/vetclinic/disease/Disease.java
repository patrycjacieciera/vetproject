package com.patrycjacieciera.vetclinic.disease;

import com.patrycjacieciera.vetclinic.diagnosis.Diagnosis;

import javax.persistence.*;
import java.util.List;

@Entity
public class Disease {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "disease_generator")
    @SequenceGenerator(name = "disease_generator", sequenceName = "disease_seq")
    private Long id;

    @Column
    private String name;

    public Disease() {

    }

    public Disease(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
