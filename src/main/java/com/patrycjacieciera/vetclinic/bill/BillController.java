package com.patrycjacieciera.vetclinic.bill;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/bill")
@RequiredArgsConstructor
public class BillController {

    private final BillService billService;

    @PostMapping
    public Long addBill(@Valid @RequestBody CreateBillRequest request){
        return billService.createBill(request);
    }

    @GetMapping("/{id}")
    public Bill getBill(@PathVariable("id") Long id){
        return billService.getBill(id);
    }
}