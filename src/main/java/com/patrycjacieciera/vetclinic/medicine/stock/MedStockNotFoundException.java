package com.patrycjacieciera.vetclinic.medicine.stock;

public class MedStockNotFoundException extends RuntimeException {

    MedStockNotFoundException (Long id){
        super(String.format("MedStock with id %d not found", id));
    }
}
