package com.patrycjacieciera.vetclinic.pet;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DiagnosisDto {

    @NotBlank
    private String description;

    @NotBlank
    private String comments;

    @NotNull
    private Long diseaseId;

    @NotNull
    private Long vetId;
}
