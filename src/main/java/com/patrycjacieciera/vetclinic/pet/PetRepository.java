package com.patrycjacieciera.vetclinic.pet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface PetRepository extends JpaRepository<Pet, Long> {

    @Query("SELECT t FROM Pet t WHERE t.name= ?1")
    List<Pet> findByName(String name);
}