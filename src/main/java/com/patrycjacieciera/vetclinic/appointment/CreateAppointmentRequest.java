package com.patrycjacieciera.vetclinic.appointment;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateAppointmentRequest {

    @NotNull
    private Long billId;
}
