package com.patrycjacieciera.vetclinic.client;

import com.patrycjacieciera.vetclinic.pet.Sex;
import lombok.Data;

@Data
public class PetDto {

    private String name;
    private String race;
    private Sex sex;
    private String color;
    private boolean purebred;
    private boolean castrated;
    private boolean alive;
    private Long owner_id;
}
