package com.patrycjacieciera.vetclinic.medicine;

public class MedicineNotFoundException extends RuntimeException {

    MedicineNotFoundException (Long id){
        super(String.format("Medicine with id %d not found", id));
    }
}
