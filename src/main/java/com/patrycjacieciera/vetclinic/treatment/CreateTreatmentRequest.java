package com.patrycjacieciera.vetclinic.treatment;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
class CreateTreatmentRequest {

    @NotNull
    private int quantity;

    @NotNull
    private MedicineDto medcineDto;

}
