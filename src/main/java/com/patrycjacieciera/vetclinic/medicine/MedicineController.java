package com.patrycjacieciera.vetclinic.medicine;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/med")
@RequiredArgsConstructor
public class MedicineController {

    private final MedicineService medicineService;

    @PostMapping
    public Long addMedicine(@Valid @RequestBody CreateMedicineRequest request) {
        return medicineService.createMedicine(request);
    }

    @GetMapping("/{id}")
    public Medicine getMedicine(@PathVariable("id") Long id) {
        return medicineService.getMedicine(id);
    }
}
