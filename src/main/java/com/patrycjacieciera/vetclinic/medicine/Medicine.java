package com.patrycjacieciera.vetclinic.medicine;


import com.patrycjacieciera.vetclinic.medicine.stock.MedicineStock;
import com.patrycjacieciera.vetclinic.treatment.Treatment;

import javax.persistence.*;

@Entity
public class Medicine {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medicine_generator")
    @SequenceGenerator(name = "medicine_generator", sequenceName = "medicine_seq")
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private double price;

    @ManyToOne
    private Treatment treatByMed;

    @OneToOne
    @JoinColumn(name = "stock_id")
    private MedicineStock stock;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Treatment getTreatByMed() {
        return treatByMed;
    }

    public void setTreatByMed(Treatment treatByMed) {
        this.treatByMed = treatByMed;
    }

    public MedicineStock getStock() {
        return stock;
    }

    public void setStock(MedicineStock stock) {
        this.stock = stock;
    }
}
