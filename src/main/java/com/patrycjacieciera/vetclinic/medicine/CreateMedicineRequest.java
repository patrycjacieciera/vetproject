package com.patrycjacieciera.vetclinic.medicine;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
class CreateMedicineRequest {

    @NotBlank
    private String name;

    @NotNull
    private double price;

    @NotNull
    private Long treatmenetId;

    @NotNull
    private Long stockId;
}
