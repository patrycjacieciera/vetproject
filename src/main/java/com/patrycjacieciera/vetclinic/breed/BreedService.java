package com.patrycjacieciera.vetclinic.breed;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BreedService {

    private final BreedRepository breedRepository;

    public Long createBreed(CreateBreedRequest request){
        Breed breed = buildBreedEntity(request);
        breedRepository.save(breed);
        return breed.getId();
    }

    private Breed buildBreedEntity(CreateBreedRequest request){
        Breed breed = new Breed();
        breed.setName(request.getName());
        return breed;
    }

    public Breed getBreed(Long id){
        return breedRepository.findById(id).orElseThrow(()-> new BreedNotFoundException(id));
    }

}