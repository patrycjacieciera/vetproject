package com.patrycjacieciera.vetclinic.pet;

import com.patrycjacieciera.vetclinic.client.FindClientResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/pet")
@RequiredArgsConstructor
public class PetController {

    private final PetService petService;

    @PostMapping
    public Long addPet(@Valid @RequestBody CreatePetRequest request) {
        return petService.createPet(request);
    }

    @GetMapping("/{id}")
    public FindPetResponse getPet(@PathVariable("id") Long id) {
        Pet pet = petService.getPet(id);
        return FindPetResponse.builder()
                .name(pet.getName())
               // .breed(pet.getBreed())
                .castrated(pet.isCastrated())
                .alive(pet.isAlive())
                .sex(pet.getSex())
                .race(pet.getRace())
                .build();
    }

  @GetMapping("/byName/{name}")
 public FindPetResponse getPetByName(@PathVariable ("name") String name){
    Pet pet = petService.getPetByName(name);
    FindPetResponse findPetResponse = new FindPetResponse(pet.getId(), pet.getName(), pet.getRace(), pet.getSex(), pet.getColor(), pet.isPurebred(), pet.isCastrated(), pet.isAlive());
    return findPetResponse;
  }
}
