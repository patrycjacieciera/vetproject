package com.patrycjacieciera.vetclinic.breed;

public class BreedNotFoundException extends RuntimeException {

    public BreedNotFoundException(Long id){
        super(String.format("Breed with id %d not found", id));
    }
}
