package com.patrycjacieciera.vetclinic.vet;

public class VetNotFoundException extends RuntimeException {

    public VetNotFoundException(Long id) {
        super(String.format("Vet with id %d not found", id));
    }
}
