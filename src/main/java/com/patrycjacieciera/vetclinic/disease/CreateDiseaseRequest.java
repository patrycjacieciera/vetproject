package com.patrycjacieciera.vetclinic.disease;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
class CreateDiseaseRequest {

    @NotBlank
    private String name;
}
