package com.patrycjacieciera.vetclinic.diagnosis;

import com.patrycjacieciera.vetclinic.disease.Disease;
import com.patrycjacieciera.vetclinic.disease.DiseaseService;
import com.patrycjacieciera.vetclinic.treatment.Treatment;
import com.patrycjacieciera.vetclinic.treatment.TreatmentService;
import com.patrycjacieciera.vetclinic.vet.Vet;
import com.patrycjacieciera.vetclinic.vet.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class DiagnosisService {

    private final DiagnosisRepository diagnosisRepository;
    private final DiseaseService diseaseService;
    private final VetService vetService;
    private final TreatmentService treatmentService;

    public Long createDiagnosis(CreateDiagnosisRequest request){
        Diagnosis diagnosis = buildDiagnosisEntity(request);
        diagnosisRepository.save(diagnosis);
        return diagnosis.getId();
    }

    private Diagnosis buildDiagnosisEntity(CreateDiagnosisRequest request){
        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setDescription(request.getDescription());
        diagnosis.setComments(request.getComments());
        diagnosis.setDate(LocalDate.now());

        Disease disease = diseaseService.getDisease(request.getDiseaseId());
        diagnosis.setDisease(disease);

        Vet vet = vetService.getVet(request.getVetId());
        diagnosis.setVet(vet);

        Treatment treatment = treatmentService.getTreatment(request.getTreatId());
        diagnosis.setTreatment(treatment);

        return diagnosis;
    }


    public Diagnosis getDiagnosis(Long id){
        return diagnosisRepository.findById(id).orElseThrow(() -> new DiagnosisNotFoundException(id));
    }


}
