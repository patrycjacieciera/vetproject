package com.patrycjacieciera.vetclinic.appointment;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("api/appointment")
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService appointmentService;

    @PostMapping
    public Long addAppointmet(@Valid @RequestBody CreateAppointmentRequest request){
        return appointmentService.createAppointment(request);
    }

    @GetMapping("/{id}")
    public Appointment getAppointment (@PathVariable("id") Long id){
        return appointmentService.getAppointment(id);
    }
}
