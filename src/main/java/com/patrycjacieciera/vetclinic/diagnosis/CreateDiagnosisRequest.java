package com.patrycjacieciera.vetclinic.diagnosis;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
class CreateDiagnosisRequest {

    @NotBlank
    private String description;

    @NotBlank
    private String comments;

    @NotNull
    private Long diseaseId;

    @NotNull
    private Long vetId;

    @NotNull
    private Long treatId;

}
