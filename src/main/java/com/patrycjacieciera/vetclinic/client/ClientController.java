package com.patrycjacieciera.vetclinic.client;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/client")
@RequiredArgsConstructor
class ClientController {

    private final ClientService clientService;

    @PostMapping
    public Long addClient(@Valid @RequestBody CreateClientRequest request){
        return clientService.clientCreate(request);
    }

    @GetMapping("/{id}")
    public FindClientResponse getClient(@PathVariable("id") Long id){
        Client client = clientService.getClient(id);
        return FindClientResponse.builder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .address(client.getAddress())
                .build();
    }

    @GetMapping("/byName/{lastName}")
    public FindClientResponse getClinetByLastName(@PathVariable(value = "lastName") String lastname ){
        Client client = clientService.getClientByLastName(lastname);
        FindClientResponse findClientResponse = new FindClientResponse(client.getId(),client.getFirstName(), client.getLastName(), client.getEmail(), client.getAddress());
        return findClientResponse;
        // TODO: jak wypisac wszystko z list??
    }

}
