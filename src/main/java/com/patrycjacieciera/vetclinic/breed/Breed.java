package com.patrycjacieciera.vetclinic.breed;

import com.patrycjacieciera.vetclinic.pet.Pet;

import javax.persistence.*;
import java.util.List;

@Entity
public class Breed {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "breed_generator")
    @SequenceGenerator(name = "breed_generator", sequenceName = "breed_seq")
    private Long id;

    @Column(nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
