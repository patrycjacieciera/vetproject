package com.patrycjacieciera.vetclinic.breed;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
class CreateBreedRequest {

    @NotBlank
    private String name;
}
