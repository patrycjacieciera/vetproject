package com.patrycjacieciera.vetclinic.medicine.stock;


import javax.persistence.*;

@Entity
public class MedicineStock {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medicineStock_generator")
    @SequenceGenerator(name = "medicineSock_generator", sequenceName = "medicineStock_seq")
    private Long id;

    @Column(nullable = false)
    private double quantity;

    public Long getId() {
        return id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
}
