package com.patrycjacieciera.vetclinic.vet;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
class CreateVetRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String specialization;

}
