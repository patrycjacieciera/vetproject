package com.patrycjacieciera.vetclinic.diagnosis;

public class DiagnosisNotFoundException extends RuntimeException {

    public DiagnosisNotFoundException(Long id){
        super(String.format("Diagnosis with id %d not found", id));
    }
}
