package com.patrycjacieciera.vetclinic.bill;

import javax.persistence.*;

@Entity
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_generator")
    @SequenceGenerator(name = "bill_generator", sequenceName = "bill_seq")
    private Long id;

    @Column (nullable = false)
    private double cost;

    public Long getId() {
        return id;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
