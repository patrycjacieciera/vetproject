package com.patrycjacieciera.vetclinic.vet;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VetService {

    private final VetRepository vetRepository;

    public Long createVet(CreateVetRequest request) {
        Vet vet = buildVetEntity(request);
        vetRepository.save(vet);
        return vet.getId();
    }

    private Vet buildVetEntity(CreateVetRequest request) {
        Vet vet = new Vet();
        vet.setFirstName(request.getFirstName());
        vet.setLastName(request.getLastName());
        vet.setSpecialization(request.getSpecialization());
        return vet;
    }

    public Vet getVet(Long id) {
        return vetRepository.findById(id).orElseThrow(() -> new VetNotFoundException(id));
    }

}