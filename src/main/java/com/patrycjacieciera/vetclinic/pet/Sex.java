package com.patrycjacieciera.vetclinic.pet;

public enum Sex {

    MALE, FEMALE
}
