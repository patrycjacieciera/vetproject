package com.patrycjacieciera.vetclinic.disease;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DiseaseRepository extends JpaRepository <Disease, Long> {
}
