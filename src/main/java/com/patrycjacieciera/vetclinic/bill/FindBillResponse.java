package com.patrycjacieciera.vetclinic.bill;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindBillResponse {

    private double cost;
}
