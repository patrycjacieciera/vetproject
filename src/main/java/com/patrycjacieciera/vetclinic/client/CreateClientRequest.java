package com.patrycjacieciera.vetclinic.client;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
class CreateClientRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String email;

    @NotBlank
    private String password;

    @NotBlank
    private String address;
    private PetDto petDto;

}
