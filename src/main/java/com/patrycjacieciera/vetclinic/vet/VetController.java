package com.patrycjacieciera.vetclinic.vet;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/vet")
@RequiredArgsConstructor
public class VetController {

    private final VetService vetService;

    @PostMapping
    public Long addVet(@Valid @RequestBody CreateVetRequest request) {
        return vetService.createVet(request);
    }

    @GetMapping("{id}")
    public Vet getVet (@PathVariable ("id") Long id){
        return vetService.getVet(id);
    }

}
