package com.patrycjacieciera.vetclinic.disease;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@RestController
@RequestMapping("/api/disease")
@RequiredArgsConstructor
class DiseaseController {

    private final DiseaseService diseaseService;

    @PostMapping
    public Long addDisease(@Valid @RequestBody CreateDiseaseRequest request){
        return diseaseService.createDisease(request);
    }

    @GetMapping("/{id}")
    public Disease getDisease(@RequestBody Long id){
        return diseaseService.getDisease(id);
    }
}
