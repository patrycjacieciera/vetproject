package com.patrycjacieciera.vetclinic.pet;

import com.patrycjacieciera.vetclinic.breed.Breed;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
class FindPetResponse {

    private Long id;
    private String name;
    private String race;
    private Sex sex;
    private String color;
    private boolean purebred;
    private boolean castrated;
    private boolean alive;
   // private Breed breed;

}
