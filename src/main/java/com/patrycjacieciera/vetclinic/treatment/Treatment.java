package com.patrycjacieciera.vetclinic.treatment;

import com.patrycjacieciera.vetclinic.medicine.Medicine;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Treatment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "treatment_generator")
    @SequenceGenerator(name = "treatment_generator", sequenceName = "treatment_seq")
    private Long id;

    @Column(nullable = false)
    private int quantity;

    @OneToMany(mappedBy = "treatByMed", cascade = CascadeType.ALL)
    private List<Medicine> medicines = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }

    public void setMedicines(List<Medicine> medicines) {
        this.medicines = medicines;
    }
}
