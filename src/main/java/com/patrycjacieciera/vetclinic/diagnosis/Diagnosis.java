package com.patrycjacieciera.vetclinic.diagnosis;

import com.patrycjacieciera.vetclinic.disease.Disease;
import com.patrycjacieciera.vetclinic.treatment.Treatment;
import com.patrycjacieciera.vetclinic.vet.Vet;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Diagnosis {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
    @SequenceGenerator(name = "client_generator", sequenceName = "client_seq")
    private long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String comments;

    @Column(nullable = false)
    private LocalDate date;

    @ManyToOne
    private Disease disease;

    @ManyToOne
    private Vet vet;

    @ManyToOne
    private Treatment treatment;

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public void setDisease(Disease disease) {
        this.disease = disease;
    }

    public Vet getVet() {
        return vet;
    }

    public void setVet(Vet vet) {
        this.vet = vet;
    }

    public Treatment getTreatment() {
        return treatment;
    }

    public void setTreatment(Treatment treatment) {
        this.treatment = treatment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
