package com.patrycjacieciera.vetclinic.medicine.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/stock")
@RequiredArgsConstructor
public class MedicineStockController {

    private final MedicineStockService medicineStockService;

    @PostMapping
    public Long addStock (@Valid @RequestBody CreateMedicineStockRequest request){
        return medicineStockService.createMedicineStock(request);
    }

    @GetMapping("/{id}")
    public MedicineStock getStock (@PathVariable ("id") Long id){
        return medicineStockService.getMedicineStock(id);
    }

}
