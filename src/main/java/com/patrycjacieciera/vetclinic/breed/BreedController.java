package com.patrycjacieciera.vetclinic.breed;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/breed")
@RequiredArgsConstructor
public class BreedController {

    public final BreedService breedService;

    @PostMapping
    public Long addBreed(@Valid @RequestBody CreateBreedRequest request) {
        return breedService.createBreed(request);
    }

    @GetMapping("/{id}")
    public Breed getBreed(@PathVariable("id") Long id) {
        return breedService.getBreed(id);
    }

}