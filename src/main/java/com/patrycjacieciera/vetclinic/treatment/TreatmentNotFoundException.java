package com.patrycjacieciera.vetclinic.treatment;

public class TreatmentNotFoundException extends RuntimeException {

    TreatmentNotFoundException(Long id){
        super(String.format("Treatment with id %d not found", id));
    }
}
