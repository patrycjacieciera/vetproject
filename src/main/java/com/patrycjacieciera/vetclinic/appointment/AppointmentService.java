package com.patrycjacieciera.vetclinic.appointment;


import com.patrycjacieciera.vetclinic.bill.Bill;
import com.patrycjacieciera.vetclinic.bill.BillService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final BillService billService;


    public Long createAppointment(CreateAppointmentRequest request){
        Appointment appointment = bulidAppointmentEntity(request);
        appointmentRepository.save(appointment);
        return appointment.getId();
    }

    private Appointment bulidAppointmentEntity(CreateAppointmentRequest request){
        Appointment appointment = new Appointment();
        appointment.setStartDateTime(LocalDateTime.now());
        appointment.setEndDateTime(LocalDateTime.now().plusMinutes(30));
        Bill bill = billService.getBill(request.getBillId());
        appointment.setBill(bill);
        return appointment;
    }

    public Appointment getAppointment(Long id){
        return appointmentRepository.findById(id).orElseThrow(() -> new AppointmentNotFoundException(id));
    }
}
