package com.patrycjacieciera.vetclinic.diagnosis;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("api/diagnosis")
@RequiredArgsConstructor
class DiagnosisController {

    private final DiagnosisService diagnosisService;

    @PostMapping
    public Long addDiagnosis(@Valid @RequestBody CreateDiagnosisRequest request){
        return diagnosisService.createDiagnosis(request);
    }

    @GetMapping("/{id}")
    public Diagnosis getDiagnosis(@PathVariable ("id") Long id){
        return diagnosisService.getDiagnosis(id);
    }

}
